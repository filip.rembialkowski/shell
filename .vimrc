" This must be set as first option
set nocompatible " vi improved

" ===================================================================
" includes
" ===================================================================
"source $VIMRUNTIME/vimrc_example.vim
"source $VIMRUNTIME/plugin/tohtml.vim

" ===================================================================
" general settings
" ===================================================================
syntax on
filetype plugin indent on

colorscheme evening

" ===================================================================
" options
" ===================================================================
"set   autochdir " change current directory on buffer change
" (not useful for developers :()
set   autoindent " have indentation copied down lines
set noautowrite " Automatically save before commands like :next and :make
set   background=dark
set   backspace=eol,start,indent
set   backup
set   backupdir=~/.vim/backups,.
set noerrorbells " errorbells: damn this beep!  ;-)
set   expandtab
set   foldminlines=5 " this does not work in 7.0 centos vim
set   guifont=Monospace\ 11
set   guioptions+=b
set   history=1000 " have this many lines of command-line (etc) history
set   hlsearch " Highlight searched term
set   ignorecase
set noincsearch " show the `best match so far' as search strings are typed
set   laststatus=2 " always show status line
set   listchars=extends:>,precedes:<
set   modeline      " let files override this .vimrc
set   modelines=100
set   mouse=nv      " enable mouse in normal and visual modes
set   number        " show line numbers
set   pastetoggle=<F6> " toggle 'paste' mode (no autoindent et al)
set   ruler " Show the line and column numbers of the cursor 
set   scrolloff=3   " Try to show few lines of context around the cursor
set   shell=bash
set   shiftround    " Round indent to multiple of 'shiftwidth'
set   shiftwidth=4  " Use indents of 4 spaces for auto/smart indenting
set   shortmess=a   " abbreviate most common messages
set   showcmd       " Show current uncompleted command
set   showmatch " Show matching brackets
set   showmode      " Show the current mode
set   sidescroll=0
set   sidescrolloff=3
set   smartcase
set   smartindent
set   smarttab
set   softtabstop=4
set   splitbelow
set   splitright
set   tabstop=4      " have 'normal' tab 4 spaces wide
set   tags=~/.vim/tags,./tags,tags,./TAGS,TAGS,~/.tags
set   textwidth=98
set   visualbell
set   whichwrap=h,l,~,[,]
set   wildchar=<TAB> " the char used for "expansion" on the command line
set   wildmode=list:longest,full
set nowrap

" remember all of these between sessions:
" %100    this many recent buffers (unless started with file arguments)
" '100    marks in this many recent files
" <5000   maximum # of lines in each register
" s1000   maximum size of a register in Kbytes
" f1      remember global marks (A-Z) 
" /500    remember 500 search terms
" don't remember marks in files if yo don't know ow to use them
" h       don't rehighlight old search patterns

"set viminfo=%100,'500,f1,/500,h,r/mnt,r/media
" original:
"set viminfo=%,'500,/500,<5000,h,s50
"set viminfo=%,'500,/500,<5000,f0,h,s50
"set viminfo=%100,'500,<1000,s1000,:500,h,f0,n~/.viminfo
set viminfo=%100,'500,<1000,s1000,:500,h,f0


" ===================================================================
" functions and autocommands
" ===================================================================
function! ToggleOption (option)
    execute 'set ' . a:option . '!'
    execute 'echo "' . a:option . ':" strpart("offon",3*&' . a:option .  ',3)'
endfunction

autocmd BufRead,BufNewFile *.tt2 set filetype=tt2html
autocmd BufNewFile,BufRead *.tt  set filetype=tt2html

" ===================================================================
" abbreviations
" ===================================================================

" Abbreviations for some important numbers:
 iab Npi 3.1415926535897932384626433832795028841972
 iab Ne  2.7182818284590452353602874713526624977573

" First, some command to add date stamps (with and without time).
" I use these manually after a substantial change to a webpage.
" [These abbreviations are used with the mapping for ",L".]
"
iab Ydate <C-R>=strftime("%Y-%m-%d")<CR>
" 2006-02-14
iab Ytime <C-R>=strftime("%H:%M")<CR>
" 22:26
iab YDT   <C-R>=strftime("%Y-%m-%d %H:%M")<CR>
" 2006-02-14 22:26
iab YDATE <C-R>=strftime("%A, %d %B %Y %T %Z")<CR>
" wtorek, 14 luty 2006 22:26:36 CET

" ===================================================================
" mappings
" ===================================================================

" When I let Vim write the current buffer I frequently mistype the
" command ":w" as ":W" - so I have to remap it to correct this typo:
  map :W :w
" same for quitting Vim
  map :Q :q
" Are you used to the Unix commands "alias" and "which"?
" I sometimes use these to look up my abbreviations and mappings.
" So I need them available on the command line:
  map :alias map
  map :which map
"     #b = "browse" - send selected URL to Netscape
  vmap #b y:!firefox "<C-R>""
" C comment
  vmap sc "zdi/* <C-R>z */<ESC>

" command to open file under cursor in another window
map gw <Esc>:sp %<CR> gf

map H :let &hlsearch = !&hlsearch<CR>
map M :set mouse=a<CR>
map N :set mouse=<CR>

" ===================================================================
" variables
" ===================================================================
"perl_fold does not work with perl folding functions (FR 2011-02-02)
let perl_fold=1

let SVNCommandEnableBufferSetup=1
let SVNCommandCommitOnWrite=1
let SVNCommandEdit='split'
let SVNCommandNameResultBuffers=1
"let SVNCommandAutoSVK='svk'

"http://superuser.com/questions/139620/lost-left-right-cursor-keys-in-vim-insert-mode
let g:omni_sql_no_default_maps = 1

command! -nargs=1 -range SuperRetab <line1>,<line2>s/\v%(^ *)@<= {<args>}/\t/g

set showbreak=»\ 

" With the default backslash leader key, typing \c will toggle highlighting on and off. That makes
" it easy to locate the cursor after scrolling in a large file.
:nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>
