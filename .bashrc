# ~/.bashrc: executed by bash(1) for non-login shells.
# When  an  interactive  shell that is not a login shell is started, bash
# reads and executes commands from /etc/bash.bashrc and ~/.bashrc,
# if these files exist.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# for fancy prompts, see .bash_functions
export PS1='\w \$ '

# History settings. See bash(1)
HISTCONTROL=ignoreboth
shopt -s histappend
export HISTSIZE=20000
export HISTFILESIZE=100000
export HISTTIMEFORMAT="%F %R "

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# Alias definitions.
[ -f ~/.bash_aliases ] && source ~/.bash_aliases

# Function definitions.
[ -f ~/.bash_functions ] && source ~/.bash_functions

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Don't wait for job termination notification
# set -o notify

# Use case-insensitive filename globbing
shopt -s nocaseglob

# When changing directory small typos can be ignored by bash
# for example, cd /vr/lgo/apaache would find /var/log/apache
shopt -s cdspell

### LOCALE ###

# set base locale
if [[ "$OSTYPE" == cygwin* ]]; then
    export LC_ALL=en_US
else
    export LC_ALL=en_US.UTF-8
fi
# set some variables to POSIX
export LC_NUMERIC=POSIX
export LC_TIME=POSIX
export LC_PAPER=POSIX
export LC_MEASUREMENT=POSIX

# use GPG for SSH auth
# if [ -f "${HOME}/.gpg-agent-info" ]; then
#     source "${HOME}/.gpg-agent-info"
#     export GPG_AGENT_INFO
#     export SSH_AUTH_SOCK
#     export SSH_AGENT_PID
# else
#     eval $(gpg-agent --daemon)
# fi

export EDITOR=vim
export VISUAL=${EDITOR}

export PAGER=less
export LESS="-FMRSX -h9999 -i -x4 -y9999"

# Auto-logout when running as root
if test `id -u` == "0" && test "$OSTYPE" != "cygwin"; then
    TMOUT=$[60*90]
else
    unset TMOUT
fi
export TMOUT

# C/C++ compiler flags
#export CFLAGS="-O2 -march=athlon-xp"
#export CXXFLAGS=$CFLAGS
#export HOSTCFLAGS="-O2 -march=i686"
#export HOSTCXXFLAGS=$HOSTCFLAGS

# vars for some old backup tools
#export TAPE='/dev/nst0'
#export SYBASE='/usr/local/freetds'
#export BXSHARE='/usr/local/share/bochs'

export LOGPATHS="/var/log"
if [ -d /opt/homebrew/var/log ]; then
    export LOGPATHS="/opt/homebrew/var/log $LOGPATHS"
fi
# could also try /service/*/log, /var/lib/**/*.log, /usr/local/**/logs"

# per-user locatedb
if test -r "$HOME/.locatedb"; then
    export LOCATE_PATH="$HOME/.locatedb"
fi

# silence bc
export BC_ENV_ARGS=--quiet

### set java path if Sun java present
if test -d "/usr/lib/jvm/java-6-sun"; then
    export JAVA_HOME="/usr/lib/jvm/java-6-sun"
    export PATH="$PATH:$JAVA_HOME/bin"
fi

# set PATH so it includes user's private bin if it exists
for bindir in ~/bin ~/.local/bin; do
    [ -d $bindir ] && export PATH="$PATH:$bindir"
done

# do the same with MANPATH
[ -d ~/man ] && export MANPATH="$MANPATH:~/man"

# Prefer some paths on MacOS
if [[ "$OSTYPE" == darwin* ]]; then
	for macpath in "/opt/homebrew/bin" "/opt/homebrew/opt/coreutils/libexec/gnubin"; do
		if [ -d "$macpath" ]; then
			export PATH="$macpath:$PATH"
		fi
	done
fi

# Load Homebrew settings on MacOS
if command -v brew >/dev/null; then
	eval "$(brew shellenv)"
	HOMEBREW_PREFIX="$(brew --prefix)"
	if [[ -r "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh" ]]; then
		source "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh"
	else
		for COMPLETION in "${HOMEBREW_PREFIX}/etc/bash_completion.d/"*; do
			[[ -r "${COMPLETION}" ]] && source "${COMPLETION}"
		done
	fi
fi



# always run ssh-agent
#if ! test "$SSH_AGENT_PID" || ! kill -0 $SSH_AGENT_PID ; then
#    eval `ssh-agent`
#    ssh-add
#fi

# Neo4j env vars
# export NEO4J_HOME=/home/neo4j/neo4j

# TeX Live env vars
if [ -d "$HOME/Aplikacje/texlive/2018" ]; then
    export TEXLIVE="$HOME/Aplikacje/texlive/2018"
    export MANPATH="$MANPATH:$TEXLIVE/texmf-dist/doc/man"
    export INFOPATH="$INFOPATH:$TEXLIVE/texmf-dist/doc/info"
    export PATH="$PATH:$TEXLIVE/bin/x86_64-linux"
fi

# Use pyenv for Python if present
if command -v pyenv >/dev/null; then
  eval "$(pyenv init -)"
fi

# Completions

# Google cloud SDK
GCSDK="$HOME/Aplikacje/google-cloud-sdk"
if [ -d "$GCSDK" ]; then
  # The next line updates PATH for the Google Cloud SDK.
  if [ -f "$GCSDK/path.bash.inc" ]; then . "$GCSDK/path.bash.inc"; fi
  # The next line enables shell command completion for gcloud.
  if [ -f "$GCSDK/completion.bash.inc" ]; then . "$GCSDK/completion.bash.inc"; fi
fi

if type kubectl &>/dev/null && ! type __start_kubectl &>/dev/null; then
    source <(kubectl completion bash)
    export PS1="\$(kubectl config current-context 2>/dev/null) $PS1"
fi

if type gh &>/dev/null && ! type __start_gh &>/dev/null; then
    source <(gh completion --shell bash)
fi

if type pip &>/dev/null && ! type -t _pip_completion &>/dev/null; then
    source <(pip completion --bash)
fi

if type aws &>/dev/null && ! type __start_aws &>/dev/null; then
    complete -C '/usr/local/bin/aws_completer' aws
fi

# Custom user settings
if [ -f ~/.bashrc_local ]; then
    source ~/.bashrc_local
fi

# Setting PATH for Python 3.13
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.13/bin:${PATH}"
export PATH
