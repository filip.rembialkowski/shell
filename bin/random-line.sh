#!/bin/bash
file=$1
[ "$file" ] && [ -r "$file" -o "$file" == "-" ] || {
	echo "Usage: $0 FILE [WANT_LINES]" >&2
	exit 1
}
want_lines=$2
[[ "$want_lines" =~ ^[0-9]+$ ]] || {
	echo "WANT_LINES must be positive integer" >&2
	exit 1
}
if [ "$file" == "-" ]; then
	shuf | head -n $want_lines
else
	shuf $file | head -n $want_lines
fi

