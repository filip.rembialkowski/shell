#!/usr/bin/perl -w
use v5.18;
use Time::HiRes qw(time usleep);

my $every = 1000000;
my $index = 0;
my @mem;
my $t0 = time;
my $t1 = $t0;

while (1) {
	push @mem, rand;
	if ( @mem % $every == 1 ) {

		my $duration = time - $t0;
		my $lps      = int( $index / $duration );

		my $duration_loop = time - $t1;
		my $lps_loop      = int( $every / $duration_loop );

		my $rss = `ps -o rss= $$`;
		die "error running `ps`" if $?;
		chomp $rss;

		say "LPS = $lps; LPS now = $lps_loop; rss = $rss KiB";

		usleep 100;

		$t1 = time;
	} ## end if ( @mem % $every == ...)
	$index++;
} ## end while (1)
