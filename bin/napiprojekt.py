#!/usr/bin/env python
#
# Script downloads subtitles from napiprojekt
#
# based on older script
# by gim,krzynio,dosiu,hash 2oo8.
# 4pc0h f0rc3

import argparse
import hashlib
import logging
import os
import re
import subprocess
import tempfile
import urllib.parse
import urllib.request
from pathlib import Path

logging.basicConfig(
    format="%(asctime)s %(levelname)s %(module)s %(funcName)s %(message)s",
    level=logging.INFO,
)

logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("movie", nargs="+", type=Path)
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    for movie in args.movie:
        download_subtitle(movie)


def download_subtitle(movie: Path) -> None:
    url = napiurl(movie)
    logger.debug("Downloading %s", url)
    content_7z = urllib.request.urlopen(url).read()
    logger.debug(f"First 10 chars of content: |{str(content_7z)[:10]}|")
    if re.search(string=str(content_7z), pattern="NPc0"):
        logger.error("NOT FOUND")
        return
    logger.debug("Uncompressing content of length %d", len(content_7z))
    content = un7zip(content_7z, password=os.environ.get("NAPIPROJEKT_ZIP_PASS"))
    logger.debug("Uncompressed content of length %d", len(content))

    subtitles = Path(movie).with_suffix(".txt")
    # Don't override the same subtitles
    if subtitles.exists() and subtitles.read_bytes() == content:
        logger.info("File %s already exists with same content", subtitles)
        return

    logger.info("Saving subtitles to file %s", subtitles)
    subtitles.write_bytes(content)


def napiurl(movie: Path) -> str:
    api_url = "http://napiprojekt.pl/unit_napisy/dl.php"
    moviehasher = hashlib.md5()
    with movie.open(mode="rb") as moviefile:
        moviehasher.update(moviefile.read(10485760))
    movieid = moviehasher.hexdigest()
    logger.debug("MD5 of first 10 MBytes: %s", movieid)
    api_token = np_token_for_hash(movieid)
    logger.debug("Napiprojekt token: %s", api_token)
    api_params = {
        "l": "PL",
        "f": movieid,
        "t": api_token,
        "v": "other",
        "kolejka": "false",
        "nick": os.environ.get("NAPIPROJEKT_NICK"),
        "pass": os.environ.get("NAPIPROJEKT_PASS"),
        "napios": os.name,
    }
    return api_url + "?" + urllib.parse.urlencode(api_params)


def np_token_for_hash(moviehash: str) -> str:
    idx = [0xE, 0x3, 0x6, 0x8, 0x2]
    mul = [2, 2, 5, 4, 3]
    add = [0, 0xD, 0x10, 0xB, 0x5]
    return "".join(
        f"{int(moviehash[(base := a + int(moviehash[i], 16)) : base + 2], 16) * m % 16:x}"
        for a, m, i in zip(add, mul, idx)
    )


def un7zip(archive, password=None, tmpfileprefix="un7zip", tmpfilesuffix=".7z") -> str:
    tmpfile = tempfile.NamedTemporaryFile(
        prefix=tmpfileprefix,
        suffix=tmpfilesuffix,
        delete=False,
    )
    tmpfile.write(archive)
    tmpfile.close()

    cmd = ["7z", "x", "-y", "-so"]
    if password is not None:
        cmd += ["-p" + password]
    cmd += [tmpfile.name]
    proc = subprocess.run(cmd, capture_output=True, check=True)
    os.remove(tmpfile.name)
    return proc.stdout


if __name__ == "__main__":
    main()
