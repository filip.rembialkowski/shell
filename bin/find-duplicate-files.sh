#!/bin/bash
dir=$1
if ! test -d "$dir"; then
	echo "Usage: $0 DIR"
	exit 1
fi

perlcode='
while ( my $input = <> ) {
    # Hex md5, followed by  2 spaces and a filename.
    if ( $input =~ /^(?<md5>[0-9a-f]{32})  (?<file>.*)$/ ) {
        push @{ $FILES_BY_MD5{ $+{md5} } }, $+{file};
    }
}
my @dups = grep { @{ $FILES_BY_MD5{$_} } > 1 } keys %FILES_BY_MD5;
if (@dups) {
    print qq{#!/bin/bash}, $/;
    for my $dup ( sort { $FILES_BY_MD5{$a}[0] cmp $FILES_BY_MD5{$b}[0] } @dups ) {
        my @files = sort { length $a <=> length $b } @{ $FILES_BY_MD5{$dup} };
        # Save the first file name
        $first = shift @files;
        print qq{##### "$first"}, $/;
        # Kill remaining duplicates
        print map { qq{rm -v "$_"} . $/ } @files;    # Watch for quotes in filenames
    }
}
'

find $dir -type f -exec md5sum {} '+' | perl -e "$perlcode"
