#!/bin/bash
# gnome-shell MINI PUDZIAN
for pid in `pidof gnome-shell`; do
  rss=`ps -p $pid -o 'rss='`
  if [[ $rss -ge 1024*1024*2 ]]; then
    DISPLAY=:1 busctl --user call org.gnome.Shell /org/gnome/Shell org.gnome.Shell Eval s 'Meta.restart("Restarting...")' >/dev/null
    break
  fi
done
