#!/bin/bash

function pidfile_start () {
	local pidfile=$1
	if [ -e "$pidfile" ]; then
		pid=$(head -n1 "$pidfile")
		if [[ $pid =~ ^-?[0-9]+$ ]] && kill -0 $pid; then
			echo "already running with pid $pid" >&2
			ps uw $pid
			exit 1
		fi
		if ! [ -w "$pidfile" ]; then
			echo "no permission to write pidfile $pidfile" >&2
			exit 2
		fi
	fi
	echo $$ > "$pidfile"
}

function pidfile_stop () {
	local pidfile=$1
	rm -f "$pidfile"
}
