#!/usr/bin/env perl
use strict;
use warnings;
use feature qw(say state switch);

my $prog = PgQueryBench->new;
$prog->initialize;
$prog->run;
exit;

package PgQueryBench;

use Getopt::Long qw(GetOptions);
use DBI;
use Time::HiRes qw(time);
use List::Util qw(shuffle);
use List::MoreUtils qw(uniq);
use Test::Deep::NoTest qw(eq_deeply);
use English qw( -no_match_vars );
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
$Data::Dumper::Terse    = 1;

my $VERSION = '0.4';

sub new {
	bless {}, shift;
}

sub get_dbh {
	my $self = shift;
	my $dsn  = "dbi:Pg:";
	$dsn .= join ';',
		map  { $_ . '=' . $self->{ config }{ $_ } }
		grep { defined $self->{ config }{ $_ } } qw(host port dbname);
	return DBI->connect( $dsn, $self->{ config }{ username }, undef, { RaiseError => 1 } );
}

sub initialize {
	my $self = shift;

	$self->{ config } = { debug => 0 };
	my $status = GetOptions(
		$self->{ config }, 'host|h=s', 'port|p=i', 'username|U=s',
		'dbname|d=s',      'help|?',   'debug+',   'iterations|i=i',
		'query|q=s@',
	);
	$self->show_help_and_die() unless $status;
	$self->show_help_and_die() if $self->{ config }{ help };

	# here you should validate params and run initializations specific to this program

	die "Please provide non-zero --iterations, and one or more non-empty --query\n"
		unless $self->{ config }{ query } and $self->{ config }{ iterations };

	$Data::Dumper::Sortkeys = 1;
	print "DEBUG: config after initialize: " . Dumper( $self->{ config } )
		if $self->{ config }{ debug };

} ## end sub initialize

sub slurp_file {
	my ( $self, $file ) = @_;
	( $file and -f $file and -r $file ) or die qq{Not a readable file: "$file"\n};
	my $fd;
	open( $fd, '<', $file ) or die qq{Can not open "$file": $!\n};
	local $/ = undef;
	my $data .= <$fd>;
	close $fd;
	return $data;
}

sub read_query_from_file {
	my ( $self, $f ) = @_;
	my $fdata = $self->slurp_file( $f );
	# remove single-line comments
	$fdata =~ s{^\s*--.*$}{}gm;
	$fdata =~ s{\s+--\s.*$}{}gm;
	# remove psql meta-commands
	$fdata =~ s{^\\.*$}{}gm;
	# collapse newlines & tabs into spaces
	$fdata =~ s{\s+}{ }gsm;
	return "/*$f*/ " . $fdata;
}

sub run {
	my ( $self ) = @_;

	my $dbh = $self->get_dbh;

	my $sth;
	my $timer;
	my $querydata;
	my $rows;
	my $winner;

	my @querylist = uniq map { -f ( $_ ) ? $self->read_query_from_file( $_ ) : $_ }
		uniq @{ $self->{ config }{ query } };

	for ( my $i = 0; $i < $self->{ config }{ iterations }; $i++ ) {
		my ( $winnertime, $winnerquery );
		foreach my $query ( shuffle @querylist ) {
			my $t0 = time;
			$sth = $dbh->prepare( $query );
			$sth->execute;
			my $aref  = $sth->fetchall_arrayref;
			my $t1    = time;
			my $delta = $t1 - $t0;
			if ( not defined $winnerquery or $delta < $winnertime ) {
				$winnerquery = $query;
				$winnertime  = $delta;
			}
			$timer->{ $query } += $delta;
			$timer->{ TOTAL }  += $delta;
			$rows->{ $query }  += scalar @$aref;
			$i == 0 and $querydata->{ $query } = $aref;
		} ## end foreach my $query ( shuffle...)
		$winner->{ $winnerquery } += 1;
	} ## end for ( my $i = 0; $i < $self...)

	say "POSTGRESQL QUERY BENCHMARK RESULTS:\n";
	say "Iterations: $self->{config}{iterations}";

	sub num {
		return sprintf( "%.3f", shift );
	}

	foreach my $query ( @querylist ) {
		my $avgms = 1000 * $timer->{ $query } / $self->{ config }{ iterations };
		say "Query:   $query";
		my $timepct = sprintf( '%d%%', 100 * $timer->{ $query } / $timer->{ TOTAL } );
		say "Time:    " . num( $timer->{ $query } ) . " s ($timepct)";
		say "Average: " . num( $avgms ) . " ms";
		say "Rows:    " . $rows->{ $query };
		say "Data:    " . Dumper( $querydata->{ $query } ) if $self->{ config }{ debug } >= 2;
		$winner->{ $query } ||= 0;
		my $winpct
			= sprintf( '%d%%', 100 * $winner->{ $query } / $self->{ config }{ iterations } );
		say "Winner:  " . $winner->{ $query } . " times ($winpct)";
	}

} ## end sub run

sub show_help_and_die {
	my $self = shift;
	my ( $format, @args ) = @_;
	if ( $format ) {
		$format =~ s/\s*\z/\n\n/;
		printf STDERR $format, @args;
	}
	print STDERR <<_END_OF_HELP_;
Syntax:
    $PROGRAM_NAME <options>

Options:
    --host             : -h : database server host or socket directory
    --port             : -p : database server port
    --username         : -U : database user name
    --dbname           : -d : database name to connect to
    --help             : -? : shows this help page
    --iterations       : -i : number of iterations
    --query            : -q : Query for benchmark (multiple allowed)

_END_OF_HELP_

} ## end sub show_help_and_die

