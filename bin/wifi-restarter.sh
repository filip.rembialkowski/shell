#!/bin/bash

# $ nmcli -g type,name con show
# vpn:xmargin
# 802-11-wireless:bubu


function _get_active_wifi () {
	nmcli -g "type,name" con show --active | grep -vw vpn | head -n 1 | cut -d: -f2-
}

while true; do
	wifi_name=$(_get_active_wifi)
	if [ "$wifi_name" ]; then
		echo "Ready to bring up $wifi_name if needed..."
		t0=$(date +%s)
		while true; do
			t1=$(date +%s)
			test $((t1-t0)) -gt 60 && break
			if ip route get 8.8.8.8 >/dev/null; then
				echo -n "$(date +%T) ok. "
			else
				nmcli con up $wifi_name
			fi
			t0=$t1
			sleep 2
		done
	else
		echo "No active Wifi connections."
	fi
	sleep 2
done
